import request from '@/utils/request'

/* 标准格式*/
// 对于登录这个接口的基础封装
// 参数 + 逻辑 + 返回值组成的
// 参数：提供接口的传给后端的参数
// 逻辑：执行一下接口请求（接口文档）
// 返回值：login函数执行结果返回值就是一个promise对象

// 通过分析 这个函数是封装了接口调用 同时返回一个promise对象
// 则在业务组件中调用此函数可以进行数据获取 或者是数据上传

// 好处
// 1. 每一个文件就是一个业务对应的所有服务请求接口汇总，清晰明了，查找方便 （方便维护）
// 2. 任何一个业务组件中如果想使用哪个接口直接导入即可 （方便复用）
// 3. 语义化清晰，维护方便 （命名即注释）
// 封装 = 复用 + 可维护性

export function login(data) {
  // reques函数执行之后的返回值是什么？
  // axios() -> Promise对象
  return request({
    url: '/sys/login',
    method: 'POST',
    data
  })
}

// 获取用户基本信息

export function getUserInfo() {
  return request({
    url: '/sys/profile',
    method: 'POST'
  })
}

/**
 * @description: 获取用户头像
 * @param {*} id 用户id
 * @return {*}
 */
export function getUserDetailById(id) {
  return request({
    url: `/sys/user/${id}`
  })
}
