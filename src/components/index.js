// 组件全局插件的开发
import PageTools from '@/components/PageTools'
import UploadImg from '@/components/UploadImg'
import ScreenFull from './ScreenFull'
import Lang from './Lang'
// 1. 如何编写插件
// 方法一：对象写法 内部必须有一个方法 install
const componentPlugin = {
  install(Vue) {
    // Vue构造函数
    // 很多专门用来做全局事情的api
    // 全局指令注册 Vue.directive
    // 全局组件注册 Vue.component
    // 原型方法挂载 Vue.prototype
    // 使用这里的全局方法按照需求编写插件逻辑
    console.dir(Vue)
    // 全局注册pageTool
    // 流程：main.js Vue.use方法 -> 执行插件install方法注入实参Vue ->
    // Vue.component执行 -> page-tolls就会被注册为全局组件
    Vue.component('page-tools', PageTools)
    Vue.component('upload-img', UploadImg)
    Vue.component('ScreenFull', ScreenFull)
    Vue.component('lang-component', Lang)
  }
}

// 2. 注册插件

// Vue.use(componentPlugin)
// 当调用use方法进行注册 自动执行插件对象内部的install方法并且把实参Vue传入

export default componentPlugin

/**
 插件和组件
  1. 针对的维度
     组件 针对于业务模块 （html + js + css）
     插件 针对于vue本身  (vue本身为了给开发者暴露一个可扩展的口子 Vue[component/directive/use] )

  2. 解决的问题
     组件 复用 + 可维护性
     插件 针对于vue做功能增强和补充  vue + router(插件) + vuex(插件)
 */
