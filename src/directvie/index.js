// 全局指令的封装
import Vue from 'vue'
import store from '@/store'
Vue.directive('btn-show', {
  // 在指令挂载的元素完成渲染时自动触发执行
  // <el-button v-my-directive="xx"></el-button>
  inserted: function(el, binding) {
    // el: 指令绑定的dom元素
    // binding: 指令参数对象 value参数很重要 指令等于号后面绑定的表达式的值
    console.log(el, binding)
    // 核心逻辑：控制当前button dom显示和隐藏  + binding.value
    // 1. 拿到当前用户按钮权限列表  useInfo
    const points = store.state.user.userInfo.roles.points
    // 2. 拿到当前按钮的权限值  binding.value
    // 3. 判断当前binding.value能否在 useInfo.roles.points找到 找到就显示 找不到就隐藏
    if (!points.includes(binding.value)) {
      // 移除真实dom
      el.remove()
    }
  }
})
