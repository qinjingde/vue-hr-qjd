import Vue from 'vue'
// 导入用来渲染图标的svg组件
import SvgIcon from '@/components/SvgIcon'

// 全局组件注册 把svg-icon组件注册为全局可用
// 1. 组件的名字  如果要用 <svg-icon/>
// 2. 组件的配置项 export default { data(){},methods:{}}
Vue.component('svg-icon', SvgIcon)

// 意义：把svg这个目录下所有的图标都引入到我们的项目中
// 变成可用的状态
// 目的：统一把所有的图标一次性导入 方便任何一个任务组件在任何地方使用svg图标
const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)
