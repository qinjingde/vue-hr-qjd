import Vue from 'vue'
// 样式初始化
import 'normalize.css/normalize.css'
// 初始化ElementUI 全局的初始化
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

// 全局样式
import '@/styles/index.scss'

// 根组件App  vuex Store  路由
import App from './App'
import store from './store'
import router from './router'

// 引入图标字体 让所有的组件中都可以使用提供好的svg图标
import '@/icons'
// 引入权限文件 (在整个项目初始化的时候就把权限代码执行掉)
// 模块 export import 如果光引入一个js文件相当于直接对立面的代码做执行处理
import '@/permission'

// Vue.use? 注册插件 很多的组件中使用elementUI组件
Vue.use(ElementUI)

// 导入插件
import componentPlugin from '@/components'
Vue.use(componentPlugin)
// 导入执行全局指令注册
import '@/directvie'
import i18n from '@/lang'

Vue.config.productionTip = false

// vue应用实例化
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App) // 渲染根组件App
})
