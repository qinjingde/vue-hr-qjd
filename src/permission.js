// 这里编写路由权限控制代码
console.log('权限控制执行了')
// 权限？
// token 有token正常获取数据 没有token话跳回到登录页面

// 人资：
// 菜单权限控制(根据不同角色登录看到不同的菜单显示)
// 按钮权限控制(根据不同的角色控制按钮的显示和隐藏)

// 本次项目中所有有关权限控制的代码都放在这里

import router from '@/router'
import store from '@/store'
import NProgress from 'nprogress'
import { asyncRoutes } from '@/router/asyncRoutes'
import 'nprogress/nprogress.css'

// 不需要token就可以放行的就叫做白名单
const WIHTELIST = ['/login', '/404']
router.beforeEach(async(to, from, next) => {
  // to: 去往的路由对象
  // from: 从哪里来的那个路由对象
  // next: 放行函数 每个分支中都要有放行函数
  // 获取token
  // 开始进度条
  NProgress.start()
  const token = store.state.user.token
  if (token) {
    // 有token
    if (to.path === '/login') {
      // 去往login 跳回到首页
      next('/')
    } else {
      next()
      // 在这里既可以保证token是有的 而且这里控制有token正常路由的跳转控制
      // 并且因为用户信息里有权限控制相关的数据 所以放在这里触发action函数
      // 最合适
      // 每次只要有路由跳转 就都会执行 存在重复的请求
      // 优化手段：如果已经请求到用户信息数据了 就不要在发送请求了
      // 如果查找vuex中user userId 如果在 不发送 不在才发生
      const userId = store.getters.userId
      if (!userId) {
        const res = await store.dispatch('user/fetchUserInfo') // 获取roles权限数据的接口
        // 数据筛选 以实际的菜单权限数组作为标准 对本地所有的动态路由做过滤去里
        const menus = res.roles.menus
        console.log(menus, asyncRoutes)
        // 数组过滤 filter
        const filterRoutes = asyncRoutes.filter(route => {
          // 需要做判断 把满足条件的route返回
          // route对象中有一个标识如果能在menus数组中找到 就可以有资格进入
          // 这里有严格的匹配 一切要以menus为主
          if (menus.includes(route.children[0].name)) {
            return route
          }
        })
        console.log('过滤之后的新数组', filterRoutes)
        // 1.动态添加到路由系统
        router.addRoutes([...filterRoutes, { path: '*', redirect: '/404', hidden: true }])
        // 2. 调用mutation函数存入vuex
        store.commit('menu/setMenuList', filterRoutes)
      }
    }
  } else {
    // 没有token
    // 判断是否在白名单内 - 能否在数组中找到 - 数组的哪个方法？
    if (WIHTELIST.includes(to.path)) {
      next()
    } else {
      next('/login')
    }
  }
  // 结束进度条
  NProgress.done()
})

