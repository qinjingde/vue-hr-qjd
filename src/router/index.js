import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import Layout from '@/layout'
// 导入动态路由表
import { asyncRoutes } from './asyncRoutes'

// 静态路由表
export const constantRoutes = [
  {
    path: '/login',
    // 路由懒加载: 只有当前正式要用到路由的时候才去获取它对应的js资源
    component: () => import('@/views/Login/index'),
    hidden: true
  },
  // 员工管理
  {
    path: '/employeedetail',
    component: Layout,
    hidden: true,
    children: [{
      path: '',
      name: 'employeedetail',
      component: () => import('@/views/Employee/Detail'),
      meta: { title: '员工详情', icon: 'people' }
    }]
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,

    children: [{
      path: '',
      name: 'Dashboard',
      component: () => import('@/views/Dashboard/index'),
      // 路由元信息 放置一些除了基础信息(path+component)以外的信息
      meta: { title: 'DashBoard', icon: 'dashboard' }
    }]
  },
  {
    path: '/import',
    component: Layout,
    hidden: true,
    children: [{
      path: '',
      name: 'import',
      component: () => import('@/views/ImportExcel'),
      // 路由元信息 放置一些除了基础信息(path+component)以外的信息
      meta: { title: 'excel导入', icon: 'dashboard' }
    }]
  }
  // 404 page must be placed at the end !!!

]

const createRouter = () => new Router({
  // 控制路由跳转的行为: 在每个路由跳转之后自动将页面滚动到顶部
  scrollBehavior: () => ({ y: 0 }),
  routes: [...constantRoutes, ...asyncRoutes] // 测试的时候可以先复原
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
// 函数: 重置整个路由实例对象
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router

// 路由补充（必须知道）
// 1. 路由跳转行为控制
// 2. 路由懒加载 import() - 网路层面的优化 用到才加载 减少项目首次打开的时间
// 3. 路由元信息配置项叫做meta - 放置一些路由额外的信息标识
// 4. 二级路由path置空 - 当前的二级路由作为一级路由的默认二级渲染 浏览器中输入的一级路由的path
// 不需要添加二级路由的路径就可以渲染出二级路由的页面
