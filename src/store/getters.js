// 全局getters做路径映射
// 意义：把子模块中需要通过很长的路径才能访问到的属性经过getter的一个
// 映射处理 方便组件中进行使用
// 常规使用：this.state.user.userInfo.info.msg.name
// getters: this.getters.username
const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  userId: state => state.user.userInfo.userId,
  username: state => state.user.userInfo.username,
  photo: state => state.user.userInfo.staffPhoto
}
export default getters
