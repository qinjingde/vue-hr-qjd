import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import menu from './modules/menu'

Vue.use(Vuex)

const store = new Vuex.Store({
  // 组合所有子模块！！！
  modules: {
    app,
    settings,
    user,
    menu
  },
  // 全局getters
  getters
})

export default store
