// 菜单数据管理模块
import { constantRoutes } from '@/router'
export default {
  namespaced: true,
  state: {
    list: [...constantRoutes] // 初始值可以把静态的路由表做为初始值
  },
  mutations: {
    // 有了动态筛选到的路由表之后 调用函数加入list
    setMenuList(state, asyncFilterRoutes) {
      state.list = [...constantRoutes, ...asyncFilterRoutes]
    }
  }
}
