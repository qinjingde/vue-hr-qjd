// 用户相关的模块
// token userInfo
import { login, getUserInfo, getUserDetailById } from '@/api/user'
import { setToken, getToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'
export default {
  // 命名空间 如果要上模块化 命名空间一般都需要开启
  // 开启这个之后 触发mutation/action
  // this.$store.diapatch('user/login', {token: '123'})
  namespaced: true,
  // 定义状态(响应式的)
  state: {
    token: getToken() || '',
    userInfo: {} // 用户
  },
  // 定义改变状态的方法(支持同步)
  mutations: {
    setToken(state, token) {
      state.token = token
      // 存入token到cookies
      setToken(token)
    },
    setUserInfo(state, userInfo) {
      state.userInfo = userInfo
    },
    removeInfo(state) {
      state.token = ''
      state.userInfo = {}
      removeToken()
      // 清空路由记录 放置下一个用户和当前用户产生权限冲突
      resetRouter()
    }
  },
  // 封装异步请求
  actions: {
    // 封装登录action
    async fetchLogin(ctx, data) {
      // 调用登录接口
      const token = await login(data)
      // 调用mutation修改数据
      ctx.commit('setToken', token)
      // 这里只放置和数据操作相关的代码
    },
    // 封装获取用户信息
    async fetchUserInfo(ctx) {
      // 1. 调用接口
      const userInfo = await getUserInfo()
      // 2. 调用用户头像接口
      const imgUserInfo = await getUserDetailById(userInfo.userId)
      // 3. 把俩个接口拿到的数据做汇总，然后调用mutation存入数据
      // js如何合并俩个对象？
      ctx.commit('setUserInfo', { ...userInfo, ...imgUserInfo })
      // 如果你想要action函数掉完之后拿到它的返回值 只需要把数据return出去即可
      return userInfo
    }
  }
}
