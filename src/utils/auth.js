import Cookies from 'js-cookie'

const TokenKey = 'hr-token'

// 取值
export function getToken() {
  return Cookies.get(TokenKey)
}
// 存值
export function setToken(token) {
  return Cookies.set(TokenKey, token)
}
// 删
export function removeToken() {
  return Cookies.remove(TokenKey)
}
